const express = require('express')
const app = express()
const path = require('path')
// const request = require('request')

app.use(express.static(path.join(__dirname, '../dist')))

app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Content-Type,Content-Length, Authorization, Accept,X-Requested-With')
  res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
  res.header('X-Powered-By', '3.2.1')
  if (req.method === 'OPTIONS') {
    res.sendStatus(200)
  } else {
    next()
  }
})

app.get('/app', function (req, res) {
  res.send('666')
})

app.listen(9090)

// var http = require('http');

// http.createServer(function (reqeust, response) {
//     response.end('Hello Node');
// }).listen(8080)
