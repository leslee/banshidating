import Vue from 'vue'
import Vuex from 'vuex'

import {http, secondURL} from '../util/httpclient.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    mobile: '',
    validateCode: '',
    idCard: '',
    // mobile: '13169840505',
    // validateCode: '123456',
    // idCard: '44078219960611191',
    userInfo: { // userId
      name: '未登录'
    },
    editableUserInfo: {
      phoneNumber: '',
      collectAddress: '',
      email: '',
      emergency: '',
      business: ''
    },
    tips: '',
    showTips: false,
    showLoading: false,
    // 全部事项
    todolist: [],
    crunmbList: [],
    // 包含字段和内容的表格
    // tableDataLsit: [],
    commonTableList: [
      {
        itemKey: 'operator_zh_name',
        itemValue: '',
        type: 'INPUT',
        name: '经营者中文名称'
      },
      {
        itemKey: 'operator_en_name',
        itemValue: '',
        type: 'INPUT',
        name: '经营者英文名称'
      },
      {
        itemKey: 'company_code',
        itemValue: '',
        type: 'INPUT',
        name: '组织机构代码'
      },
      {
        itemKey: 'live_address',
        itemValue: '',
        type: 'INPUT',
        name: '住所'
      },
      {
        itemKey: 'product_address',
        itemValue: '',
        type: 'INPUT',
        name: '经营场所(中文)'
      },
      {
        itemKey: 'product_en_address',
        itemValue: '',
        type: 'INPUT',
        name: '经营场所(英文)'
      },
      {
        itemKey: 'contact_phone',
        itemValue: '',
        type: 'INPUT',
        name: '联系电话'
      },
      {
        itemKey: 'contact_fax',
        itemValue: '',
        type: 'INPUT',
        name: '联系传真'
      },
      {
        itemKey: 'postalcode',
        itemValue: '',
        type: 'INPUT',
        name: '邮政编码'
      },
      {
        itemKey: 'email',
        itemValue: '',
        type: 'INPUT',
        name: '电子邮件'
      },
      {
        itemKey: 'business_register_date',
        itemValue: '',
        type: 'INPUT',
        name: '工商登记注册日期'
      },
      {
        itemKey: 'business_register_code',
        itemValue: '',
        type: 'INPUT',
        name: '工商登记注册号'
      }
    ],
    registerBusiness: [
      {
        itemKey: 'corporation_name',
        itemValue: '',
        name: '企业法定代表人姓名',
        type: 'INPUT'
      },
      {
        itemKey: 'valid_certificates_number',
        itemValue: '',
        name: '有效证件号',
        type: 'INPUT'
      },
      {
        itemKey: 'registered_capital',
        itemValue: '',
        name: '注册资金',
        type: 'INPUT'
      },
      {
        itemKey: 'remark',
        itemValue: '',
        name: '备注',
        type: 'INPUT'
      }
    ],
    foreignerRegisterBusiness: [
      {
        itemKey: 'corporation_name_or_individual_business_leader',
        itemValue: '',
        name: '企业法定代表人/个体工商负责人姓名',
        type: 'INPUT'
      },
      {
        itemKey: 'corporation_name_or_individual_business_leader_number',
        itemValue: '',
        name: '有效证件号',
        type: 'INPUT'
      },
      {
        itemKey: 'enterprise_assets_or_personal_property',
        itemValue: '',
        name: '企业资产/个人财产',
        type: 'INPUT'
      },
      {
        itemKey: 'remark',
        itemValue: '',
        name: '备注',
        type: 'INPUT'
      }
    ],
    wxReady: false,
    // jsSdkConfig: {},
    location: {
      province: "",
      city: "",
      district: ""
    }
  },
  mutations: {
    updateLocation (state, message) {
      state.location = message
    },
    // updataJsSdkConfig (state, message) {
    //   state.jsSdkConfig = message
    // },
    updataEditableUserInfo (state, message) {
      state.editableUserInfo[message.type] = message.value
    },
    changeTipsStatus (state, message) {
      state.showTips = !state.showTips
    },
    updataTips (state, message) {
      state.tips = message
    },
    changeLoadingStatus (state, message) {
      state.showLoading = message
    },
    updateMobile (state, mobile) {
      state.mobile = mobile
    },
    updateidCard (state, idCard) {
      state.idCard = idCard
    },
    updateValidate (state, validate) {
      state.validateCode = validate
    },
    updateUserInfo (state, userInfo) {
      state.userInfo = userInfo
    },
    updateTodolist (state, todoList) {
      state.todolist = todoList
    },
    updateCrumbList (state, crumb) {
      state.crunmbList.push(crumb)
    },
    // 下面是那个 tabledata 的数据保存 mutations
    catchCommonTableList (state, commonval) {
      commonval.forEach((backfill) => {
        state.commonTableList.forEach((common) => {
          if (backfill.itemKey === common.itemKey) {
            common.itemValue = backfill.itemValue
          }
        })
      })
    },
    updateCommonTableList (state, message) {
      state.commonTableList[message.idx]['itemValue'] = message.val
    },
    updataRegisterBusiness (state, message) {
      state.registerBusiness[message.idx]['itemValue'] = message.val
    },
    updataForeignerRegisterBusiness (state, message) {
      state.foreignerRegisterBusiness[message.idx]['itemValue'] = message.val
    },
    catchTableDataObj: function (state, val) {
      val.forEach(element => {
        state.tableDataObj[element.itemKey] = element.itemValue
      })
    },
    clearCollectAdress (state, message) {
      state.editableUserInfo.collectAddress = ''
    },
    updataWxReadyStatus (state, message) {
      state.wxReady = message
    }
    // updatetableList (state, message) {
    //   state.todolist = message
    // }
  },
  getters: {
    wait_worklist: function (state, getters) {
      return state.todolist.filter(function (todo) {
        return !todo.operStatus
      })
    },
    done_worklist: function (state, getters) {
      return state.todolist.filter(function (todo) {
        return todo.operStatus
      })
    },
    showLoginClear: function (state, getters) {
      return state.mobile
    },
    showidCardClear: function (state, getters) {
      return state.idCard
    },
    probablyLocation: function (state, getter) {
      return (state.location.province + state.location.city + state.location.district) || '请选择地点';
    }
  },
  actions: {
    // data 是请求该API 所必须的参数
    fetchTableDetail (context, data) {
      return http.get(secondURL.tableDetail, {
        tableId: data.tableId,
        userId: data.userId,
        processId: data.processId
      })
    },
    getLocation (context, data) {
      return new Promise((resolve, reject) => {
        window.wx.getLocation({
          type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
          success: function (res) {
            let latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
            let longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
            let speed = res.speed; // 速度，以米/每秒计
            let accuracy = res.accuracy; // 位置精度
            http.get(secondURL.area, {
              lat: latitude,
              lng: longitude
            }).then((res) => {
              context.commit('updateLocation', res.data);
              resolve(res.data);
            }).catch(err => {
              reject();
            })
          }
        })
      })
    }
    // fetchJsSdkConfig () {
    //   http.get(secondURL.)
    // }
  }
})
