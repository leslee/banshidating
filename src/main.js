// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import App from './App'

import router from './router'

// import './components/global.js'

import './css/base.css'
import './css/lesbase.scss'

import {DatetimePicker, Picker} from 'we-vue'
import 'we-vue/lib/style.css'
import {http, secondURL} from './util/httpclient.js'

import store from './store/'

import Loading from './components/loading/loading'

import VeeValidate from 'vee-validate'

require('es6-promise').polyfill();


Vue.use(DatetimePicker);
Vue.use(Picker);
Vue.use(VeeValidate);


Vue.directive('countdown', {
  inserted: function (el, binding) {
    // 聚焦元素
    // console.log(el, binding)
  }
})

Vue.component('loading', Loading)

Vue.prototype.$http = http
Vue.prototype.$secondURL = secondURL
Vue.prototype.$getFormatDate = function (data) {

  let innerDate = new Date(data.replace(/\-/g, '/'))
  var seperator1 = '-'
  var year = innerDate.getFullYear()
  var month = innerDate.getMonth() + 1
  var strDate = innerDate.getDate()
  if (month >= 1 && month <= 9) {
    month = '0' + month
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = '0' + strDate
  }
  return year + '' + seperator1 + month + '' + seperator1 + strDate + '' ? year + seperator1 + month + seperator1 + strDate : '时间获取失败'
}

Vue.config.productionTip = false

/* eslint-disable no-new */
window.vueVM = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
