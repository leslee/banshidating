import axios from 'axios'
import qs from 'qs'
import store from '../store/index'
// axios.defaults.baseURL = 'http://47.106.102.231'
// axios.defaults.baseURL = 'http://192.168.0.103:8002'
// axios.defaults.baseURL = 'http://www.xqintcloud.com';
axios.defaults.baseURL = 'http://192.168.0.102:8002';

const secondURL = {
  // 事项分类(大类)
  itemCategory: '/elechall/itemCategory/list',
  // 事项分类列表(小分类) 这个可以搜索  参数: {pageNumber: '', pageSize: '', itemCategoryId: '', serverType: '', name: ''}
  item: '/elechall/item/list',
  // 用户登录/授权
  user: '/elechall/user/webLogin',
  // 登出
  loginOut: '/elechall/user/loginOut',
  // 验证码短信发送
  sendSMS: '/elechall/user/sendSMS',
  // 保存用户
  saveUser: '/elechall/user/insert',
  // 修改用户
  editUser: '/elechall/user/modify',
  // 代办事项（未完成）
  // userWork: '/elechall/userWork/list',
  // 代办事项（分页）
  page_userwork: '/elechall/userWork/getPage',
  // 获取热门内容,只需要填写一个参数: serverType
  getHotItem: '/elechall/item/getHotItem',
  // 获取填写材料的列表,
  tableList: '/elechall/form/table/list',
  // 保存办理记录，这里的连接在填写表格的时候，就要发送请求，拿processId，为保存表格做铺垫
  save: '/elechall/userWork/save',
  // 保存表格以下两个要同时提交
  tableSubmit: '/gthall/form/tableSubmit',
  // 提交表单, 保存表单 参数: {processId: '', userId: '', tableId: ''}
  formSubmit: '/elechall/form/formSubmit',
  // 回填表格
  tableDetail: '/elechall/form/table/detail',
  // 保存表格
  saveTableData: '/elechall/form/table/save',
  // 授权状态下拿userId
  info: '/elechall/user/info',
  // 获取用户信息
  get: '/elechall/user/get',
  // 微信授权接口 带个 jumoUrl 参数
  auth: '/elechall/auth',
  // sdkConfig 带个url参数
  jsSdkConfig: '/elechall/getJsSdkConfig',
  // 拿 公司列表  /elechall/userCompany/list 参数: userId
  userCompany: '/elechall/userCompany/list',
  // 拿用户地址的(公司地址) /elechall/userAddress/list 参数: userId
  userAddress: '/elechall/userAddress/list',
  // 根据经纬度获得具体地址 ,参数是 lat lng
  area: '/elechall/getArea'
};
const statusCode = [200, 304];
const http = {
  get (url, data = {}) {
    // 出现loading
    return new Promise(function (resolve, reject) {
      // store.commit('changeLoadingStatus', true)
      axios({
        method: 'get',
        url: url,
        params: data,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).then(function (res) {
        // 取消loading
        // store.commit('changeLoadingStatus', false)
        if (statusCode.includes(res.status)) {
          // console.log(res, '请求成功')
          if (res.data.status === '000') {
            resolve(res.data)
          } else if (res.data.status === '-1') {
            store.commit('updataTips', res.data.message);
            console.log('status', res.data);
            store.commit('changeTipsStatus');
            console.log(res.data.message)
          } else if (res.data.status === '-2') {
            // window.sessionStorage.setItem();
            store.commit('updataTips', res.data.message);
            store.commit('changeTipsStatus');
            // axios({
            //   method: 'get',
            //   url: secondURL.auth,
            //   params: {
            //     // jumpUrl: window.location.href
            //     jumpUrl: window.location.href
            //   }
            // }).then((res) => {
            //   if (res.status === 200) {
            //     window.location.href = res.data.data
            //   }
            // }).catch(res => {
            //
            // })
          }
          // else if (res.data.status === '-3') {
          //   window.vueVM.$router.push('/login');
          // }
        }
      }).catch(function (err) {
        // store.commit('changeLoadingStatus', false)
        // console.log(err, '请求失败')
        reject(err);
        console.log(err);
        store.commit('updataTips', '网络小哥开小差了');
        store.commit('changeTipsStatus');
      })
    })
  },
  post (url, data = {}) {
    // showloading
    // store.commit('changeLoadingStatus', true)
    return new Promise(function (resolve, reject) {
      axios({
        method: 'post',
        url: url,
        data: qs.stringify(data),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(function (res) {
        // store.commit('changeLoadingStatus', false)
        // 取消loading
        if (statusCode.includes(res.status)) {
          resolve(res.data)
        }
      }).catch(function (err) {
        // store.commit('changeLoadingStatus', false)
        // todo 处理错误
        reject(err)
      })
    })
  }
};

// axios.interceptors.response.use(function (response) {
//   // 对响应数据做点什么
//   console.log(response, 'res')
//   return response
// }, function (error) {
//   // 对响应错误做点什么
//   return Promise.reject(error)
// })

export {
  secondURL,
  http
}
