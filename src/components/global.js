// 此文件用于动态的引入所有的vue组件
import Vue from 'vue'

function capitalizeFirstLetter (string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}
function pathFilter (path) {
  return path.substr(path.lastIndexOf('/') + 1)
}
// require.context 可以搜索上下文的文件
const requireComponent = require.context(
  '.', true, /\.vue$/
)
console.dir(requireComponent)
// 循环注册所有的vue文件再也不用手动注册了
requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)
  // 处理componentName
  const filename = fileName
  const lowerName = pathFilter(filename)
  const componentName = capitalizeFirstLetter(lowerName.replace(/^\.\//, '').replace(/\.\w+$/, '').replace(/_/g, ''))
  console.log(componentName)
  Vue.component(componentName, componentConfig.default || componentConfig)
})
