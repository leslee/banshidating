import Vue from 'vue'

import Router from 'vue-router'

import Home from '@/components/home/home'

import Search from '@/components/search/search'

import store from '../store/'

// import {http, secondURL} from '../util/httpclient.js'

Vue.use(Router)
const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/enterprise',
      children: [
        {
          path: 'enterprise',
          component: reslove => {
            return require(['@/components/home/_business/_business'], reslove)
          }
        },
        {
          path: 'personal',
          beforeEnter: (to, from, next) => {
            // console.log(to.components.defaule)
            // console.log(to)
            next()
          },
          component: reslove => {
            return require(['@/components/home/_business/_business'], reslove)
          }
        }
      ],
      beforeEnter: (to, from, next) => {
        next()
      }
    },
    {
      path: '/login',
      name: 'login',
      component: reslove => {
        return require(['@/components/login/login'], reslove)
      }
    },
    {
      path: '/location',
      name: 'location',
      component: reslove => {
        return require(['@/components/location/location'], reslove)
      }
    },
    {
      path: '/appointment',
      name: 'appointment',
      component: reslove => {
        return require(['@/components/appointment/appointment'], reslove)
      },
      children: [
        {
          path: 'success',
          name: 'success',
          component: reslove => {
            return require(['@/components/appointment/_success/_success'], reslove)
          },
          children: [
            {
              path: 'state',
              name: 'state',
              component: reslove => {
                return require(['@/components/appointment/_success/__state/__state'], reslove)
              }
            },
            {
              path: 'map',
              name: 'map',
              component: reslove => {
                return require(['@/components/appointment/_success/__map/__map'], reslove)
              }
            }
          ]
        },
        {
          path: 'common',
          component: reslove => {
            return require(['@/components/appointment/_common/_common'], reslove)
          }
        }
      ]
    },
    {
      path: '/basemsg',
      name: 'basemsg',
      component: reslove => {
        return require(['@/components/basemsg/basemsg'], reslove)
      },
      children: [
        {
          path: 'msglist',
          component: reslove => {
            return require(['@/components/basemsg/_msglist/_msglist'], reslove)
          }
        },
        {
          path: 'commonlist',
          component: reslove => {
            return require(['@/components/basemsg/_commonlist/_commonlist'], reslove)
          }
        },
        {
          path: 'categoryradio',
          component: reslove => {
            return require(['@/components/basemsg/_categoryradio/_categoryradio'], reslove)
          }
        }
        // {
        //   path: 'categorymulti',
        //   component: reslove => {
        //     return require(['@/components/basemsg/_categorymulti/_categorymulti'], reslove)
        //   }
        // }
      ]
    },
    {
      path: '/workguide',
      name: 'workguide',
      component: reslove => {
        return require(['@/components/work_guide/work_guide'], reslove)
      },
      children: [
        {
          path: 'watch',
          component: reslove => {
            return require(['@/components/work_guide/_watch/_watch'], reslove)
          }
        },
        {
          path: 'edit',
          component: reslove => {
            return require(['@/components/work_guide/_edit/_edit'], reslove)
          }
        }
      ]
    },
    {
      path: '/category',
      name: 'category',
      component: reslove => {
        return require(['@/components/category/category'], reslove)
      },
      children: [
        {
          path: '/category/:serverType',
          component: resolve => {
            // console.log(resolve)
            return require(['@/components/category/_server_type/_server_type'], resolve)
          },
          children: [
            {
              path: '/category/:serverType/:id',
              component: resolve => {
                // console.log(resolve)
                return require(['@/components/category/_detail/_detail'], resolve)
              }
            }
          ]
          // ,
          // props: (route) => {
          //   console.log(route)
          //   return {
          //     data: '666'
          //   }
          // }
        }
      ]
    },
    {
      path: '/search',
      name: 'search',
      component: Search,
      children: [
        {
          path: '/search/hot',
          component: resolve => {
            // console.log(resolve)
            return require(['@/components/search/_hot/_hot'], resolve)
          }
        },
        {
          path: '/search/result',
          component: resolve => {
            // console.log(resolve)
            return require(['@/components/search/_result/_result'], resolve)
          }
        },
        {
          path: '/search/404',
          component: resolve => {
            // console.log(resolve)
            return require(['@/components/search/_not_search/_not_search'], resolve)
          }
        }
      ]
    },
    {
      path: '/personCenter',
      name: 'personCenter',
      component: resolve => {
        // console.log(resolve)
        return require(['@/components/personCenter/personCenter'], resolve)
      },
      children: [
        {
          path: ':opertype',
          component: resolve => {
            // console.log(resolve)
            return require(['@/components/personCenter/_worklist/_worklist'], resolve)
          }
        }
      ]
    },
    {
      path: '/personDetail',
      name: 'personDetail',
      children: [
        {
          path: 'edit',
          name: 'edit',
          component: resolve => {
            // console.log(resolve)
            return require(['@/components/personDetail/_edit/_edit'], resolve)
          },
          children: [
            {
              path: ':type',
              // name: 'editphone',
              component: resolve => {
                // console.log(resolve)
                return require(['@/components/personDetail/_edit/_edit'], resolve)
              }
            }
            // {
            //   path: 'editinfo',
            //   name: 'editinfo',
            //   component: resolve => {
            //     // console.log(resolve)
            //     return require(['@/components/personDetail/_edit_info/_edit_info'], resolve)
            //   }
            // },
            // {
            //   path: 'editbirth',
            //   name: 'editbirth',
            //   component: resolve => {
            //     // console.log(resolve)
            //     return require(['@/components/personDetail/_edit_birth/_edit_birth'], resolve)
            //   }
            // },
            // {
            //   path: 'editbusiness',
            //   name: 'editbusiness',
            //   component: resolve => {
            //     // console.log(resolve)
            //     return require(['@/components/personDetail/_edit_business/_edit_business'], resolve)
            //   }
            // }
          ]
        },
        {
          path: 'common',
          name: 'common',
          component: resolve => {
            // console.log(resolve)
            return require(['@/components/personDetail/_common/_common'], resolve)
          }
        }
      ],
      component: resolve => {
        return require(['@/components/personDetail/personDetail'], resolve)
      }
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.path.includes('basemsg')) {
    window.scrollTo(0, 0)
  }
  next();
  // if (to.path.includes('enterprise') ||
  //   to.path.includes('personal') ||
  //   to.path.includes('category') ||
  //   to.path.includes('location')) {
  //   next()
  // } else if (window.sessionStorage.getItem('loginStatus')) {
  //   next({
  //     path: '/login',
  //     replace: false
  //   })
  // } else {
  //   next()
  // }

  // if (to.path.includes('enterprise') ||
  //   to.path.includes('personal') ||
  //   to.path.includes('category') ||
  //   to.path.includes('location')) {
  //   next()
  // } else {
  //   http.get(secondURL.user, {
  //     mobile: window.sessionStorage.getItem('mobile') || '',
  //     idCard: window.sessionStorage.getItem('idCard') || '',
  //     validateCode: window.sessionStorage.getItem('validateCode') || '',
  //     openId: window.sessionStorage.getItem('openId') || ''
  //   }).then(function (res) {
  //     console.log('---进来')
  //     if (res.status !== '000') {
  //       store.commit('updataTips', '请登录')
  //       store.commit('changeTipsStatus')
  //       next({
  //         path: '/login',
  //         replace: true
  //       })
  //     } else if (res.status === '000') {
  //       store.commit('updateUserInfo', res.data)
  //       // console.log(res.data.data)
  //       next()
  //     }
  //   }).catch((err) => {
  //     console.log(err, '----------------------')
  //     store.commit('updataTips', '系统繁忙')
  //     store.commit('changeTipsStatus')
  //   })
  //   next()
  // }
})
export default router
